from tkinter import *
import webbrowser
import os

root = Tk()
root.title('RM-Overnight-Toolkit')
root.geometry('1000x10')

my_menu = Menu(root)
root.config(menu=my_menu)

# Click command
def our_command():
    webbrowser.open('tinyurl.com/RMCheatSheet')
def our_command1():
    webbrowser.open('tinyurl.com/OnCallRMTeam') 
def our_command2():
    webbrowser.open('tinyurl.com/RMTeamDashboard')      
def our_command3():
    webbrowser.open('https://tinyurl.com/RM-OvernightMinutes')
#Create menu item
file_menu = Menu(my_menu)
my_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="CheatSheet", command = our_command)
file_menu.add_separator()
file_menu.add_command(label="On-Call", command = our_command1)
file_menu.add_separator()
file_menu.add_command(label="Dashboard", command = our_command2)
file_menu.add_separator()
file_menu.add_command(label="Minutes", command = our_command3)


root.mainloop()